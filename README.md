#### Commands

```bash
# install ArgoCD in k8s
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```


```bash
# access ArgoCD UI
kubectl get svc -n argocd
kubectl port-forward -n argocd svc/argocd-server 8080:443 > /dev/null 2>&1 & # run it in background

# login with admin user and below token (as in documentation):
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo

```


